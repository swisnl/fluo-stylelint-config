# @swis/fluo-stylelint-config

> The default stylelint config for Fluo projects.

Extends [`stylelint-config-standard`](https://github.com/stylelint/stylelint-config-standard).

# Howto

Run: `npm install @swis/fluo-stylelint-config stylelint-order stylelint-scss`

In your `.stylelintrc`:
```
{
    "extends": "@swis/fluo-stylelint-config"
}
```
