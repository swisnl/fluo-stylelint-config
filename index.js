module.exports = {
    "extends": "stylelint-config-standard",
    "plugins": [
        "stylelint-order",
        "stylelint-scss"
    ],
    "rules": {
        "at-rule-no-unknown": null,
        "at-rule-no-vendor-prefix": true,
        "block-no-empty": null,
        "max-empty-lines": 1,
        "media-feature-name-no-vendor-prefix": true,
        "no-descending-specificity": null,
        "number-leading-zero": "never",
        "order/properties-alphabetical-order": true,
        "property-no-vendor-prefix": true,
        "selector-no-vendor-prefix": true,
        "scss/at-rule-no-unknown": true,
        "value-list-max-empty-lines": 1,
        "value-no-vendor-prefix": true,

        "at-rule-empty-line-before": [
            "always",
            {
                "except": [
                    "blockless-after-blockless",
                    "first-nested"
                ],
                "ignore": [
                    "after-comment"
                ],
                "ignoreAtRules": [
                    "else"
                ]
            }
        ],

        "order/order": [
            [
                "custom-properties",
                {
                    "type": "at-rule",
                    "name": "include",
                    "hasBlock": false
                },
                "declarations",
                {
                    "type": "rule",
                    "selector": "/&:.+/"
                },
                {
                    "type": "at-rule",
                    "name": "include",
                    "hasBlock": true
                }
            ],
            {
                "unspecified": "ignore"
            }
        ]
    }
}

